﻿using System;

namespace Test
{
  class Program
  {
    static void Main(string[] args)
    {
      //Bbbbb(0.0, 3);
      //Bbbbb(1.0, 3);
      //Bbbbb(-610405412.26, 1);
      //Bbbbb(0.1, 3);
      //Bbbbb(0.01, 3);
      //Bbbbb(0.001, 3);
      //Bbbbb(0.1, 2);
      //Bbbbb(0.01, 2);
      //Bbbbb(0.001, 2);
      //Bbbbb(0.11, 3);
      //Bbbbb(0.111, 3);
      //Bbbbb(0.011, 3);
      ////Aaaaa(-42);
      ////Aaaaa(0);
      ////Aaaaa(42);
      //RandomTester();
      //DoubleTester();
      //RandomConsoleWriter();
      Popis.Aaaaa();
      Console.ReadKey();
    }

    static void DoubleTester()
    {
      Random r = new Random();
      const double range = 1000000000.0;
      while (true)
      {
        Bbbbb(r.NextDouble() * 2.0 * range - range, r.Next(3, 3));
        Console.ReadKey();
      }
    }

    static void Porovnavac()
    {
      Random r = new Random();
      while (true)
      {
        Aaaaa(r.Next(0, 1000000));
        Console.ReadKey();
      }
    }

    static readonly CisloSlovy.Konvertor konvertr = new CisloSlovy.Konvertor(" ", 3, CisloSlovy.VolbyKonvertoru.Default | CisloSlovy.VolbyKonvertoru.DesetinnaCastSNulamaNaKonci);

    static private void Aaaaa(int n)
    {
      Console.WriteLine(n.ToString("N0"));
      Console.WriteLine(konvertr.NaSlova(n));
      Console.WriteLine("------");
    }

    private static void Bbbbb(double n, int mist)
    {
      Console.Write(n.ToString("N" + konvertr.DesetinnychMist.ToString()));
      Console.Write(": ");
      Console.WriteLine(konvertr.NaSlova(n));
      
    }

    static void RandomTester()
    {
      const double range = 1000000000.0;
      var rnd = new Random();
      var volbyArr = new CisloSlovy.VolbyKonvertoru[] 
      {
        CisloSlovy.VolbyKonvertoru.Default,
        CisloSlovy.VolbyKonvertoru.DesetinnaCastSNulamaNaKonci,
        CisloSlovy.VolbyKonvertoru.VzdyZobrazitCelaNula,
        CisloSlovy.VolbyKonvertoru.DesetinnaCastSNulamaNaKonci | CisloSlovy.VolbyKonvertoru.VzdyZobrazitCelaNula
      };
      while (true)
      {
        var volby = volbyArr[rnd.Next(0, volbyArr.Length - 1)];
        var desetinnychMist = rnd.Next(0, 3);
        var k = new CisloSlovy.Konvertor(" ", desetinnychMist, volby);
        var n = rnd.NextDouble() * 2.0 * range - range;
        var aaa = k.GetSlova(n);
        var aaaa = k.NaSlova(n);
      }
    }

    static void RandomConsoleWriter()
    {
      const double range = 10000.0;
      var rnd = new Random();
      var volbyArr = new CisloSlovy.VolbyKonvertoru[]
      {
        CisloSlovy.VolbyKonvertoru.Default,
        CisloSlovy.VolbyKonvertoru.DesetinnaCastSNulamaNaKonci,
        CisloSlovy.VolbyKonvertoru.VzdyZobrazitCelaNula,
        CisloSlovy.VolbyKonvertoru.DesetinnaCastSNulamaNaKonci | CisloSlovy.VolbyKonvertoru.VzdyZobrazitCelaNula
      };
      while (true)
      {
        var volby = CisloSlovy.VolbyKonvertoru.DesetinnaCastSNulamaNaKonci | CisloSlovy.VolbyKonvertoru.VzdyZobrazitCelaNula; //volbyArr[rnd.Next(0, volbyArr.Length - 1)];
        var desetinnychMist = rnd.Next(0, 3);
        var k = new CisloSlovy.Konvertor(" ", desetinnychMist, volby);
        var n = rnd.NextDouble() * 2.0 * range - range;
        Console.WriteLine(n.ToString("N" + desetinnychMist.ToString()));
        Console.WriteLine(k.NaSlova(n));
        Console.ReadKey();
      }
    }


  }
}
