﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
  class Popis
  {

    public static void Aaaaa()
    {

      // Cim oddelovat jednotliva slova.
      string separator = " ";

      // Na kolik mist zaokrouhlovat vysledne cislo. Cislo pouze od 0 do 3 (vcetne).
      int desetinnychMist = 2;

      // Dalsi podruzne volby.
      var volby = CisloSlovy.VolbyKonvertoru.VzdyZobrazitCelaNula;

      // Konvertor, ktery provadi preklad
      var konvertor = new CisloSlovy.Konvertor(separator, desetinnychMist, volby);           
      
      var piSlovy = konvertor.NaSlova(Math.PI);
      Console.WriteLine(piSlovy);
      // Vypise: tři celá čtrnáct

      var a = konvertor.NaSlova(35469.2);
      Console.WriteLine(a);
     // Vypise: třicet pět tisíc čtyři sta šedesát devět celá dva

      var b = konvertor.NaSlova(35469.0);
      Console.WriteLine(b);
      // Vypise: ticet pět tisíc čtyři sta šedesát devět celá nula
    }

  }
}
