﻿/* 
 * 
 * VZDEJTE SE VEŠKERÉ NADĚJE, VY, JENŽ SEM VSTOUPÍTE
 * 
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace CisloSlovy
{
   
  /// <summary>
  /// Umoznuje zapnout dalsi podruznosti.
  /// </summary>
  [Flags]
  public enum VolbyKonvertoru
  {

    /// <summary>
    /// Zadny spesl nastaveni.
    /// </summary>
    Default = 0,

    /// <summary>
    /// Pokud je zapnuto  napr pro 3 desetinna mista .100 => cela sto; .010 => cela nula deset
    /// </summary>
    DesetinnaCastSNulamaNaKonci = 1,

    /// <summary>
    /// Jestlize je pocet desetinnych mist > 0, tak vzdy zobrazi ke konci prida "cela nula", i kdy je desetinna cast nulova.
    /// </summary>
    VzdyZobrazitCelaNula = 2,
  }

  /// <summary>
  /// Preklada cislo na jeho slovni ceske vyjadreni.
  /// </summary>
  public class Konvertor
  {

    /// <summary>
    /// Dalsi podruzne volby.
    /// </summary>
    public VolbyKonvertoru Volby { get; }

    /// <summary>
    /// Cim oddelovat jednotliva slova.
    /// </summary>
    public string Separator { get; }

    /// <summary>
    /// Na kolik mist zaokrouhlovat vysledne cislo. Cislo pouze od 0 do 3 (vcetne).
    /// </summary>
    public int DesetinnychMist { get; }

    /// <param name="separator">Cim oddelovat jednotliva slova.</param>
    /// <param name="desetinnychMist">Na kolik mist zaokrouhlovat vysledne cislo. Cislo pouze od 0 do 3 (vcetne).</param>
    /// <param name="volby">Dalsi podruzne volby.</param>
    /// <exception cref="ArgumentOutOfRangeException" />
    public Konvertor(string separator, int desetinnychMist, VolbyKonvertoru volby)
    {
      if (desetinnychMist < 0 || desetinnychMist > 3) throw new ArgumentOutOfRangeException(nameof(desetinnychMist), "Pocet desetinnych mist musi byt 0 do 3");
      this.Volby = volby;
      this.Separator = separator ?? string.Empty;
      this.DesetinnychMist = desetinnychMist;
    }

    /// <summary>Vytvori <see cref="Konvertor"/> s vychozima volbama.</summary>
    /// <param name="separator">Cim oddelovat jednotliva slova.</param>
    /// <param name="desetinnychMist">Na kolik mist zaokrouhlovat vysledne cislo. Cislo pouze od 0 do 3 (vcetne).</param>
    /// <exception cref="ArgumentOutOfRangeException" />
    public Konvertor(string separator, int desetinnychMist) : this(separator, desetinnychMist, VolbyKonvertoru.Default)
    { }

    /// <summary>Vytvori <see cref="Konvertor"/> s vychozima volbama a na dve desetinna mista.</summary>
    /// <param name="separator">Cim oddelovat jednotliva slova.</param>
    /// <exception cref="ArgumentOutOfRangeException" />
    public Konvertor(string separator) : this(separator, 2, VolbyKonvertoru.Default)
    { }

    private const string Nula = "nula";

    private const string Cela = "celá";

    private static string GetJednicku(Rod r)
    {
      if (r == Rod.Z)
        return "jedna";
      else if (r == Rod.S)
        return "jedno";
      else
        return "jeden";
    }

    private static readonly string[][] slovnikStovky =  new string[][] {
      new string[] { null },
      new string[] { "sto" },
      new string[] { "dvě", "stě" },
      new string[] { "tři", "sta" },
      new string[] { "čtyři", "sta" },
      new string[] { "pět", "set" },
      new string[] { "šest", "set" },
      new string[] { "sedm", "set" },
      new string[] { "osm", "set" },
      new string[] { "devět", "set" }
    };

    private static readonly string[][] slovnikJednotkyADesitky_M = Konvertor.SestavitJednotkyADesitky(Rod.M);

    private static readonly string[][] slovnikJednotkyADesitky_Z = Konvertor.SestavitJednotkyADesitky(Rod.Z);

    private static string[][] SestavitJednotkyADesitky(Rod rod)
    {
      string cisloJedna = Konvertor.GetJednicku(rod);
      string[][] r = new string[100][];
      List<string> teens = new List<string>() { null, cisloJedna, "dva", "tři", "čtyři", "pět", "šest", "sedm", "osm", "devět", "deset", "jedenáct", "dvanáct", "třináct", "čtrnáct", "patnáct", "šestnáct", "sedmnáct", "osmnáct", "devatenáct" };
      for (int i = 0; i < 20; i++)
        r[i] = new string[] { teens[i] };
      string[] slovnikDesitky = new string[] { null, null, "dvacet", "třicet", "čtyřicet", "padesát", "šedesát", "sedmdesát", "osmdesát", "devadesát" };
      for (int i = 20; i < 100; i++)
        r[i] = new string[] { slovnikDesitky[i / 10], teens[i % 10] };
      return r.ToArray();
    }

    private static void SegmentCislaNaSlova(int stovky, int jednotkyADesitky, Rod rodNasledujicihoSegmentu, List<string> r)
    {
      r.AddRange(slovnikStovky[stovky].FilterNulls());
      var slovnikJednotkyADesitky = rodNasledujicihoSegmentu == Rod.M ? slovnikJednotkyADesitky_M : slovnikJednotkyADesitky_Z;
      r.AddRange(slovnikJednotkyADesitky[jednotkyADesitky].FilterNulls());
    }

    private static void SegmentCislaNaSlova(int segment, Rod rodNasledujicihoSegmentu, List<string> r) => 
      SegmentCislaNaSlova(segment / 100, segment % 100, rodNasledujicihoSegmentu, r);    
       
    private static readonly string[][] tvaryRadu = new string[][] {
      new string[] { null, null, null },
      new string[] { "tisíc", "tisíce", "tisíc" },
      new string[] { "milion", "miliony", "milionů" }
    };

    private static readonly Rod[][] rodSegmentu = new Rod[][] {
      new Rod[] { Rod.Z, Rod.Z, Rod.Z },
      new Rod[] { Rod.M, Rod.M, Rod.M },
      new Rod[] { Rod.M, Rod.M, Rod.Z }
    };

    /// <exception cref="ArgumentOutOfRangeException">jestlize je zaporne nebo vetsi nez nebo rovno miliarde</exception>
    private static void KladneCisloNaSlova(int n, List<string> r)
    {
      if (n < 0 || n >= 1000000000) throw new ArgumentOutOfRangeException(nameof(n));

      if (n == 0) r.Add(Nula);

      List<List<string>> segmenty = new List<List<string>>();
      int indexRadu = 0;
      do
      {
        int segment = n % 1000;
        int indexTvaru;
        if (segment <= 1)
          indexTvaru = 0;
        else if (segment <= 4)
          indexTvaru = 1;
        else
          indexTvaru = 2;
        List<string> segmentSlova = new List<string>();
        SegmentCislaNaSlova(segment, rodSegmentu[indexRadu][indexTvaru], segmentSlova);
        segmenty.Add(segmentSlova);
        string tvarRadu;
        if (segmentSlova.Count != 0 && (tvarRadu = tvaryRadu[indexRadu][indexTvaru]) != null)
          segmentSlova.Add(tvarRadu);
        n /= 1000;
        indexRadu++;
      } while (n > 0);

      segmenty.Reverse();
      r.AddRange(segmenty.SelectMany(w => w));

    }

    private static int OrezatNulyNaKonci(int value)
    {
      int result = value;
      while (result != 0 && Math.Round(result / 10.0) == result / 10.0)
        result /= 10;
      return result;
    }

    /// <summary>
    /// Prevede desetinnou cast na slovni vyjadresni
    /// </summary>
    private void DesetinnaCastNaSlova(int zaCarkou, List<string> r)
    {
      var pridatNul = this.DesetinnychMist;
      int a = 1;
      while(zaCarkou >= a)
      {
        pridatNul--;
        a *= 10;
      }
      if (!this.Volby.HasFlag(VolbyKonvertoru.DesetinnaCastSNulamaNaKonci))
        zaCarkou = OrezatNulyNaKonci(zaCarkou);
      r.AddRange(from i in Enumerable.Range(0, pridatNul) select Nula);
      Konvertor.KladneCisloNaSlova(zaCarkou, r);
    }

    /// <summary>
    /// Vrati jednotliva slova. Cislo zaokrouhli na <see cref="DesetinnychMist"/> (maximalne 3 mista). Pokud je absolutni cislo miliarda nebo vetsi, vyhodi vyjimku.
    /// </summary>
    /// <param name="n">Absolutni hodnota musi byt mensi, nez 10^9.</param>
    /// <exception cref="ArgumentOutOfRangeException" />
    public List<string> GetSlova(double n)
    {
      bool jeZaporne = n < 0.0;
      n = Math.Abs(Math.Round(n, this.DesetinnychMist));

      if (n >= 1000000000.0) throw new ArgumentOutOfRangeException(nameof(n), "Vice nebo mene nez miliardu neumim.");

      var words = new List<string>();
      int predCarkou = (int)n;
      int zaCarkou = (int)Math.Round(Math.Pow(10, this.DesetinnychMist) * (n - Math.Floor(n))); 

      if (jeZaporne)
        words.Add("mínus");
      Konvertor.KladneCisloNaSlova(predCarkou, words);
      if (zaCarkou > 0)
      {
        words.Add(Cela);
        this.DesetinnaCastNaSlova(zaCarkou, words);
      }
      else if (this.Volby.HasFlag(VolbyKonvertoru.VzdyZobrazitCelaNula) && this.DesetinnychMist > 0)
      {
        words.Add(Cela);
        words.Add(Nula);
      }
      return words;

    }

    /// <summary>
    /// Prevede <paramref name="n"/> na jeho slovni vyjadreni. Cislo zaokrouhli na <see cref="DesetinnychMist"/> (maximalne 3 mista). Pokud je absolutni cislo miliarda nebo vetsi, vyhodi vyjimku.
    /// </summary>
    /// <param name="n">Absolutni hodnota musi byt mensi, nez 10^9.</param>
    /// <exception cref="ArgumentOutOfRangeException" />
    public string NaSlova(double n) => string.Join(this.Separator, this.GetSlova(n));
         
  }

  #region Nejaky balast pro interni potreby

  static class Extensions
  {
    public static IEnumerable<T> FilterNulls<T>(this IEnumerable<T> items) where T : class => from i in items where i != null select i;
  }

  enum Rod { M, Z, S }

  #endregion

}
